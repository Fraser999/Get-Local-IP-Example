#![feature(convert, ip, plugin)]
#![plugin(regex_macros)]

extern crate libc;
extern crate regex;

use std::io;
use std::net::*;
use std::process::Command;
use std::str::FromStr;
use std::sync;
use std::thread;

fn get_ifconfig() -> Option<String> {
    if let Ok(output) = Command::new("ifconfig").output() {
        Some(String::from_utf8_lossy(&output.stdout).into_owned())
    } else if let Ok(output) = Command::new("ipconfig").output() {
        Some(String::from_utf8_lossy(&output.stdout).into_owned())
    } else {
        None
    }
}

fn get_v4_addresses() -> Vec<Ipv4Addr> {
    let mut all_addresses = vec![];
    let output = match get_ifconfig() {
        Some(output) => output,
        None => return all_addresses,
    };
    let v4_regex =
        regex!(r"(?:inet (?:addr:[ ]*)??|IPv4 Address[\. ]+: )((?:\d{1,3}\.){3}\d{1,3})");
    for capture in v4_regex.captures_iter(output.as_str()) {
        if let Ok(address) = Ipv4Addr::from_str(capture.at(1).unwrap_or("")) {
            if !address.is_loopback() {
                all_addresses.push(address);
            }
        }
        println!("-- {}",
                 capture.at(1).unwrap_or(""));
    }
    all_addresses
}

fn get_v6_addresses() -> Vec<Ipv6Addr> {
    let mut all_addresses = vec![];
    let output = match get_ifconfig() {
        Some(output) => output,
        None => return all_addresses,
    };
    let v6_regex =
        regex!(r"(?:inet6 (?:addr:[ ]*)??|IPv6 Address[\. ]+: )([0-9:a-fA-F]*:[0-9:a-fA-F]+)");
    for capture in v6_regex.captures_iter(output.as_str()) {
        if let Ok(address) = Ipv6Addr::from_str(capture.at(1).unwrap_or("")) {
            if !address.is_loopback() {
                all_addresses.push(address);
            }
        }
        println!("-- {}", capture.at(1).unwrap_or(""));
    }
    all_addresses
}

fn get_local_address(listener: &TcpListener) -> std::io::Result<SocketAddr> {
    let listener_local_address = try!(listener.local_addr());
    let listener_port = listener_local_address.port();
    let mut attempt_addresses: Vec<SocketAddr>;
    match listener_local_address {
        SocketAddr::V4(_) => {
            attempt_addresses = get_v4_addresses().iter()
                .map(|address| SocketAddr::V4(SocketAddrV4::new(*address, listener_port)))
                .collect();
        },
        // It's safe to ignore the flowinfo and scope_id (i.e. set them to 0) here since we'll be
        // returning the local_addr of any connection we manage to establish, and this will have the
        // correct details for these fields.
        SocketAddr::V6(_) => {
            attempt_addresses = get_v6_addresses().iter()
                .map(|address| SocketAddr::V6(SocketAddrV6::new(*address, listener_port, 0, 0)))
                .collect();
        },
    };


    // Iterate each interface and try to connect.  Once/if connected, the peer_addr yields the
    // local address.
    let mut result =
        Err(io::Error::new(io::ErrorKind::AddrNotAvailable, "Failed to get local address"));
    let (producer, consumer) = sync::mpsc::channel();
    let attempt_addresses_size = attempt_addresses.len();

    for attempt_address in attempt_addresses {
        let producer = producer.clone();
        let _ = thread::Builder::new().name("Local address worker".to_string()).spawn(move || {
                                                                          println!("TRYING {:?}", attempt_address);
            match TcpStream::connect(attempt_address.clone()) {
                Ok(stream) => {
                    match stream.local_addr() {
                        Ok(local_address) => {
                            match local_address {
                                SocketAddr::V6(address) => {
                                    let _ = producer.send(Some((SocketAddr::V6(address),
                                                                address.ip().is_global())));
                                },
                                SocketAddr::V4(address) => {
                                    let _ = producer.send(Some((SocketAddr::V4(address), true)));
                                },
                            }
                        },
                        Err(_) => {
                            let _ = producer.send(None);
                        },
                    }
                },
                _ => {
                    let _ = producer.send(None);
                },
            };
        });
    }

    for _ in 0..attempt_addresses_size {
        if let Ok(Some((address, is_global))) = consumer.recv() {
            if is_global {
                return Ok(address);
            } else {
                result = Ok(address)
            }
        }
    }

    result
}

fn main() {
    // println!("HOSTNAME: {:?}", get_env_var("HOSTNAME".to_string()));
    // println!("get_hostname: {:?}", get_hostname());
    println!("get_v6_addresses(): {:?}", get_v6_addresses());
    println!("get_v4_addresses(): {:?}", get_v4_addresses());

    if let Ok(listener_v6) = TcpListener::bind(("::", 0)) {
        println!("Local IPv6 address is {:?}", get_local_address(&listener_v6));
        let _ = thread::spawn(move || {
            loop {
                if let Ok(stream) = listener_v6.accept() {
                    println!("Connected from {:?} -- my endpoint is {:?}.  Reported peer as {:?}", stream.0.peer_addr(), stream.0.local_addr(), stream.1);
                    // thread::spawn(move|| {
                    //     handle_client(stream)
                    // });
                }
            }
        });
    } else {
        println!("Local IPv6 address not found.");
    }

    if let Ok(listener_v4) = TcpListener::bind(("0.0.0.0", 0)) {
        println!("Local IPv4 address is {:?}", get_local_address(&listener_v4));
        let _ = thread::spawn(move || {
            loop {
                if let Ok(stream) = listener_v4.accept() {
                    println!("Connected from {:?} -- my endpoint is {:?}.  Reported peer as {:?}", stream.0.peer_addr(), stream.0.local_addr(), stream.1);
                    // thread::spawn(move|| {
                    //     handle_client(stream)
                    // });
                }
            }
        });
    } else {
        println!("Local IPv4 address not found.");
    }

    thread::sleep_ms(2000);
}
